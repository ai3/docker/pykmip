FROM debian:stable-slim

ENV DEBIAN_FRONTEND=noninteractive
RUN apt-get -q update && \
    apt-get install -y --no-install-recommends python3-pykmip && \
    rm -fr /var/lib/apt/lists/*

ENTRYPOINT ['/usr/bin/pykmip-server']

